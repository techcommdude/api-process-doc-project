import requests
import os.path
import sys
import json

#
# This script pulls all Horizon projects from Gitlab while keeping the same folder structure.
#
# You will have to define GITLAB_PRIVATE_TOKEN environment variable on your Windows machine.
# See https://gitlab.dematic.com/-/profile/personal_access_tokens.
#
# Requirements:
# - Python 3.11.4 installed on your local machine and configured as an SDK for this project.
# - HORIZON_HOME environment variable tells the script where to put the cloned repository.
# - GITLAB_PRIVATE_TOKEN environment variable is set on your local Windows machine.

if __name__ == '__main__':
    GITLAB_URL: str = "https://gitlab.dematic.com"

    git_token = os.environ.get("GITLAB_PRIVATE_TOKEN")

    if not git_token:
        print("GITLAB_PRIVATE_TOKEN environment variable must be set on your local machine and in Gitlab to use this script.")
        print("See # You will have to define GITLAB_PRIVATE_TOKEN environment variable.")
        print("See https://gitlab.dematic.com/-/profile/personal_access_tokens.")
        exit(-1)

    horizon_home = os.environ.get("HORIZON_HOME")
    if not horizon_home:
        #Enter the drive you are using on your local machine
        horizon_home = "C:"

    if len(sys.argv) == 2:
        horizon_home = sys.argv[1]

    print("Using folder {horizon_home} as root folder to clone to")

    headers = {"PRIVATE-TOKEN": git_token}

    s = requests.Session()
    s.headers.update({"PRIVATE-TOKEN": git_token})

    projects = []
    
    iteration = 0
    per_page = 100 # limited to 100 per https://docs.gitlab.com/ee/api/#pagination so we have to page through the list of projects
    while True:
        # Get the web page to parse
        web_page = s.get("https://gitlab.dematic.com/api/v4/groups/horizon/projects?per_page=100&page={page}&include_subgroups=true".format(page=iteration))
        content = json.loads(web_page.content)
        iteration = iteration + 1
        
        for project in content:
            path = "{}/{}".format(horizon_home, project["path_with_namespace"])
            repo_path = project["http_url_to_repo"]
            projects.append((path, repo_path))
            
        if len(content) < per_page:
            break;

    for path, repo_path in projects:
        if os.path.exists(path):
            print(" Already cloned {}".format(repo_path))
        else:
            print(" Cloning {} into {}".format(repo_path, path))
            os.makedirs(path, exist_ok=True)
            command = 'git clone "{}" "{}"'.format(repo_path, path)
            output = os.system(command)
