[[_TOC_]]

<!---
- Use the above [[_TOC_]] tag to display the Table of Contents in GitLab.  This tag is specific to GLFM (GitLab Flavored Markdown)
- During the CI/CD process, this tag is removed using the Linux sed command for all documentation formats.
- Do not use {:target="_blank"} for hyperlinks.  It will not work in any of the output formats when using pandoc.  This is a convention that only works for HTML output from Mkdocs and Material theme.
- TODO: Figure out how to open links externally/new tab for this use case.
- Do not use any Material theme for Mkdocs syntax since this project uses GitLab Flavoured Markdown (GLFM) syntax.
- Using HTML in tables to create line breaks, ordered and unordered lists works somewhat in tables.  Doesn't really work with the HTML output, this needs to be tested further.  The output mostly works due to this extension: --from markdown-markdown_in_html_blocks+raw_html
- HTML syntax in tables works with HTML for the most part, but not PDF and Word generation. Unable to get this to work.  Just need to keep this in mind if content in table cells gets overly complicated.
- Code blocks do highlight text if you specify `java` for instance.
- Line numbering, use this:

``` {.java .numberLines}
import java.awt.Rectangle;
More java code;
```

TODO: It may make sense to rename the main documentation file to something else and just have a Readme in the project for the sake of information.
- Possibly create a doc folder where multiple files can be stored.

-->


# About this guide

This guide contains process documentation for Technical Writers that are updating the Horizon API documentation. The API documentation at Dematic is usually published in Swagger format.

## Contributing to this guide

You can edit this guide directly in this GitLab repository with one of the following methods:

* Clone the project to your local machine and edit the *Readme.md* file on your local system. You can then commit the file to GitLab.
* Use GitLab's Web IDE to edit the *Readme.md* file directly in [GitLab](https://gitlab.dematic.com/gst/api-documentation-project/blob/main/Readme.md) and commit your changes:

![](images/edit-web-ide.png)

# Overview of API documentation in the Horizon project

This repo/project outlines how to configure your environment to make API documentation updates for the Horizon project. 
For the most
part, the Java source files contain the API documentation in the form of annotations. 

The build process collects documentation/annotations and generates a `*.yaml` file for the OpenAPI specification. You can find the generated file in the _target_ directory of the Git repository:

![](images/open-api-target-directory.png)

During the R&D build process, a Jenkins job attaches the generated `*.yaml` file to a Confluence wiki page
such as these. Wiki pages are useful for determining the source project in GitLab for a particular API: 

* [https://wiki.dematic.com/display/IQH/ORD-EDGE+REST+API](https://wiki.dematic.com/display/IQH/ORD-EDGE+REST+API)
* [https://wiki.dematic.com/display/IQH/ORD-SERVICE+REST+API](https://wiki.dematic.com/display/IQH/ORD-SERVICE+REST+API)
* [https://wiki.dematic.com/display/IQH/TAS+REST+API](https://wiki.dematic.com/display/IQH/TAS+REST+API)
* [https://wiki.dematic.com/display/IQH/RCV+REST+API](https://wiki.dematic.com/display/IQH/RCV+REST+API)

The process outlined in this document helps to ensure that writers don't break builds before committing updates to the 
API documentation.

If you find content with errors or sections that need an update, please feel free to update this file with your changes.

## Download this documentation in other formats

This project uses Markdown for the source documentation. 
It's also available in PDF, Word, HTML and Confluence Markup format 
thanks to the magic of the open-source project [Pandoc](https://pandoc.org/index.html).
You can download the latest version of the
documentation as an `API-Doc-Process-Guide.zip` file [here](https://gitlab.dematic.com/gst/api-documentation-project/-/jobs).  

After downloading the zip file, extract the *Documentation* folder from the zip and view the files on your local machine.  
For convenience, the **images** folder contains all the images used in the documentation.

When opening the Word document, click **Yes** on this dialog:

![](images/Pandoc-word.png)

* [https://gitlab.dematic.com/gst/api-documentation-project/-/jobs](https://gitlab.dematic.com/gst/api-documentation-project/-/jobs)

Artifacts download page in GitLab:

![](images/pages_download.png)

Contents of the zip file:

![](images/download.png)

## Overall API documentation process summary

Here's a quick summary of the process for updating the API documentation at Dematic. This is a high-level overview only. For detailed instructions, you will need to follow the procedures outlined in this document:

1. Request access to the Horizon group in GitLab.
2. Clone all projects in the Horizon group in GitLab to your local system.
3. Configure your environment so that you can compile Java code, build with Maven, and in general decide if your changes are valid code, according to the Dematic coding standard.
4. Make your changes to the API documentation annotations in the Java file.
5. There are two options at this point:
    * Scan your API documentation updates in IntelliJ to decide if your changes meet the Dematic coding standard.
    * Run another Maven build locally to determine if your updates resulted in any build errors.
6. Once you have determined that there are no errors from either of the above tasks, you can create a branch and commit your changes according to the guidelines outlined in this documentation.
7. Create a merge request and assign it to the product owner or developer who's responsible for the feature. Depending on the level of changes, you can also get someone to review after you commit your changes. 

As mentioned previously, the following sections in this document offer more details on these steps.

# Initial environment setup and configuration

You must complete all the steps in this section to configure your environment for API documentation updates.

**Note**: The steps in these sections are by no means exhaustive. This documentation assumes some familiarity with 
API documentation and working with an IDE such as IntelliJ or Visual Studio Code. You must also be somewhat familiar with
Git, Git workflows and GitLab.

## Git and GitLab installation and configuration

Set up and configure Git and GitLab. You'll need to understand the basics of Git, Git workflow and how to 
use Git in the context of an IDE such as IntelliJ.

You must use the Git integration feature of IntelliJ IDEA. If you already use IntelliJ, this approach lets you perform 
basic Git commands right from the IDE and search the Git history. You can also edit the properties file 
and commit your changes using the same tool.

Here are some links with more information:

* [https://wiki.dematic.com/display/DEVOPS/Git+Training](https://wiki.dematic.com/display/DEVOPS/Git+Training)
* [https://wiki.dematic.com/display/GST/First+Time+Git+Setup](https://wiki.dematic.com/display/GST/First+Time+Git+Setup)
* [https://wiki.dematic.com/display/DVF/Development+Environment+Setup#DevelopmentEnvironmentSetup-Git](https://wiki.dematic.com/display/DVF/Development+Environment+Setup#DevelopmentEnvironmentSetup-Git)
* [https://www.jetbrains.com/help/idea/set-up-a-git-repository.html](https://www.jetbrains.com/help/idea/set-up-a-git-repository.html)
* [https://www.jetbrains.com/help/idea/using-git-integration.html](https://www.jetbrains.com/help/idea/using-git-integration.html)

## IntelliJ installation and configuration

This section outlines IntelliJ installation and other configuration instructions.


### Install IntelliJ

Install IntelliJ using the R&D guidelines found on the wiki—see link below. It's recommended to not use the Windows Choco 
package manager and install it manually:

* [https://wiki.dematic.com/display/IQH/Installation+for+Windows#InstallationforWindows-InstalltheJetBrainsToolboxorIntelliJIdeaUltimate](https://wiki.dematic.com/display/IQH/Installation+for+Windows#InstallationforWindows-InstalltheJetBrainsToolboxorIntelliJIdeaUltimate)

You can also activate the license using these instructions: 

* [https://wiki.dematic.com/display/DS/Configuring+your+IDE+for+Dematic+development#ConfiguringyourIDEforDematicdevelopment-Activatealicense](https://wiki.dematic.com/display/DS/Configuring+your+IDE+for+Dematic+development#ConfiguringyourIDEforDematicdevelopment-Activatealicense)

This page has some good general information on installing and configuring IntelliJ, but not everything is relevant.
It's just a good source of information:  

* [https://wiki.dematic.com/display/DS/Configuring+your+IDE+for+Dematic+development](https://wiki.dematic.com/display/DS/Configuring+your+IDE+for+Dematic+development)

### Import the settings file

In IntelliJ, import the IntelliJ settings file named *settings_IntelliJ.zip* that's found in the root of 
this repository. Use the menu options outlined in this screen capture:

![](images/import-settings.png)


### View merge requests in GitLab

Although optional, you can view GitLab merge requests in IntelliJ. See this topic for more information on how to set it up: 

* [https://www.jetbrains.com/help/idea/gitlab.html#manage-gitlab-accounts](https://www.jetbrains.com/help/idea/gitlab.html#manage-gitlab-accounts)

You must set this environment variable on your local system using the value of the token you configured in GitLab:

![](images/gitlab_private_token.png)

After setting up IntelliJ with this feature, you can view MR information from within the IDE and specify 
search criteria:

![](images/IntelliJ_MR-1.png)

You can also view a specific MR in your browser:

![](images/IntelliJ_MR-2.png)

## Request access to the Horizon group in GitLab

Request access to the Horizon group in GitLab—see link below. The button in the screen-capture below should 
read **Request Access** for new members. You will need Developer access to this group:

* [https://gitlab.dematic.com/horizon](https://gitLab.dematic.com/horizon)

**Note**: If you don't hear back using the aforementioned process, you can also request access to the GitLab Horizon group in JIRA at this URL: 

* [https://jira.dematic.com/servicedesk/customer/portal/2](https://jira.dematic.com/servicedesk/customer/portal/2)

![GitLab access to the Horizon group](images/GitlabAccess.png)


## Request access to the Horizon team in Jenkins

From within Jenkins, you can check if your committed files resulted in a successful build.

You can find the Horizon team Jenkins jobs at this URL:

* [https://cje.dematic.com/teams-horizon/blue/organizations/horizon/pipelines](https://cje.dematic.com/teams-horizon/blue/organizations/horizon/pipelines)

You can request access to the group at this URL: 

* [https://jira.dematic.com/servicedesk/customer/portal/2](https://jira.dematic.com/servicedesk/customer/portal/2)

## Install Java

Maven requires an installation of Java. Java is also used to compile your projects.

As of writing, you can install the version of Java (Amazon Corretto 18) found at this URL:

* [https://github.com/corretto/corretto-18/releases](https://github.com/corretto/corretto-18/releases)

![](images/java-jdk.png)

In your IntelliJ IDE, use the `java -version` command to confirm a successful installation
and configuration:

![](images/java-version.png)

**Notes:**

* You should remember the installation location on your machine as you may need this information.
* Install the full SDK and update the *Path* and *JAVA_HOME* environment variable if it prompts you.


  ![](images/java_home.png)

* You should set your `PATH` environment variable as part of the installation, but you may need to do
  it manually.


  ![](images/path-environment-variable.png)

## Install Python

Install Python on your local machine and run the *clone-horizon.py* script which downloads all
Horizon repositories.

**Steps:**

1. Download version 3.11.4 found at this URL:

    * [https://www.python.org/downloads/release/python-3114/](https://www.python.org/downloads/release/python-3114/)

    It's easiest to download the recommended Windows installer (`.msi` file).

   ![](images/python-installation.png)

2. To locate all installations of Python on your machine, use the `where.exe python` command in Windows PowerShell or a command window:

    ![`where.exe python` in Windows PowerShell](images/where-python-powershell.png)

3. Install Python on your local machine in a directory that you will remember. To verify the version of Python on
   your machine, run the `python --version` command in the Terminal Window in IntelliJ:

   ![Python version command](images/PythonVersionCommand.png)

4. If you don't see the version of Python on your machine that you just installed, you may need to update your
   environment variables to point to your Python installation:

   ![](images/python-path.png)

## Request access to Artifactory

Artifactory is where you can find the latest build binaries. Maven pulls these files during a build.

You can request access here: [https://jira.dematic.com/servicedesk/customer/portal/2](https://jira.dematic.com/servicedesk/customer/portal/2)

## Installing and configuring Maven and Artifactory

**Note:** Maven requires an installation of Java. You will need to check on the current Java requirements for Horizon. This is outlined earlier in this document.

This wiki page in the Horizon space has good information on how to install and configure Maven:

* [https://wiki.dematic.com/display/IQH/Apache+Maven+configuration](https://wiki.dematic.com/display/IQH/Apache+Maven+configuration)

You can also refer to this wiki page in the Dematic IQ Optimize space:

* [https://wiki.dematic.com/display/dcdng/Tools+installation#Toolsinstallation-mavenInstallApacheMaven](https://wiki.dematic.com/display/dcdng/Tools+installation#Toolsinstallation-mavenInstallApacheMaven)

These are the Maven environment variables you must set:

![](images/maven-environment.png)

This is an example of the Maven repository, the location of the repository and the location of the
*settings.xml* file.

Example of the repository location:

![](images/repository-maven.png)

Example of the contents of the repository:

![](images/maven-repository-contents.png)

## Horizon checkstyle setup

You will need to follow the instructions in this wiki for Horizon-specific information. There is other information on the wiki 
about Dematic code formatting, but the below link is specific to Horizon:

* [https://wiki.dematic.com/display/IQH/IntelliJ+configuration#IntelliJconfiguration-DownloadandconfigureCheckstyle](https://wiki.dematic.com/display/IQH/IntelliJ+configuration#IntelliJconfiguration-DownloadandconfigureCheckstyle)

You will also need to install the *Checkstyle IDEA* plugin and follow the relevant wiki instructions 
on this page: 

* [https://wiki.dematic.com/display/DS/Configuring+your+IDE+for+Dematic+development#ConfiguringyourIDEforDematicdevelopment-UseCheckstyle](https://wiki.dematic.com/display/DS/Configuring+your+IDE+for+Dematic+development#ConfiguringyourIDEforDematicdevelopment-UseCheckstyle)

![](images/checkstyle-IDEA.png)

After installation and configuration, you can select a Java file (most if not all, API documentation will be in Java source files) 
that has API documentation and do a scan to test your updates against the Dematic coding standards. If your changes obey 
Horizon coding standards, you should get a message that there are no problems in the file.

![](images/horizon_checkstyle.png)

## Clone all Horizon repositories

You can clone all Horizon repositories using the *clone-horizon.py* script found in this repository. For the most part,
you can follow the instructions in this wiki page or use it as an extra source of information when trying to complete 
the steps in this topic:

* [https://wiki.dematic.com/display/IQH/Clone+the+repositories+from+GitLab](https://wiki.dematic.com/display/IQH/Clone+the+repositories+from+GitLab)

**Additional notes:**

* As outlined in the earlier [section](#initial-environment-setup-and-configuration), you must have Java and Python installed to complete the steps in this topic.
* Some people may experience issues due to insufficient RAM on their machine. Use the Windows Task Manager to monitor how your machine utilizes RAM resources.
* The wiki page above has some useful information on how to complete this task. You should use the Python script found in this repository to download the Horizon repositories.

**Steps:**

1. Create a personal access token in GitLab per the above wiki page. Store the token value somewhere as you will need it again at some point and for the next step.

2. Next, create the `GITLAB_PRIVATE_TOKEN` environment variable on your machine with the token that you configured in GitLab.

3. Create the `HORIZON_HOME` environment variable on your machine to specify the location of your downloaded Horizon repositories. Please take note of the location.
   
4. Here's an example of the environment variables on your machine, which enables cloning of all Horizon repositories:

    ![](images/Horizon_home_variable.png)

5. Run the `python clone-horizon.py` command at the command line.
    
    ![](images/clone-horizon-python.png)
    
6. You may need to connect to GitLab if you haven't authenticated:

    ![](images/gitlab_connection.png)
    
7. You'll probably see something like this:

    ![](images/cloning-repos.png)
    
8. You can also do a `git pull` command at any time to grab the latest source files for a particular repository:

    ![](images/gitpull.png)

# API documentation update process

This section outlines some common tasks you'll need to perform as part of updating the API documentation. This section 
assumes you have already completed [setting up and configuring your environment](#initial-environment-setup-and-configuration).


## Run a build with Maven

Maven is a build tool that will pull all the latest binaries from Artifactory.

If you have a *pom.xml* file in the root of your repository, the Maven tab will be available in IntelliJ.

![](images/maven-build1.png)

**Steps:**

1. Remember to clone all repositories per this [topic](#clone-all-horizon-repositories) to get all the latest code from GitLab.
2. On a regular basis, do a `git pull` command on the repository to make sure the repository is up-to-date.
3. Sync the project and download all sources and documentation and ensure this is successful:

    ![](images/maven-build.png)

4. Run the `mvn clean install -DskipTests` command on individual repositories (most often an SCM) to grab the latest snapshot files from Artifactory and do a build. Run the command in the root of your project where you will find the main *pom.xml* file:

    ![](images/mvn_clean_install.png)

5. You should get a result like this:

    ![](images/maven-success.png)
    
6. You may get an unsuccessful build on occasion, and you may see some errors in the source code, but for the most part, you should make sure the errors aren't related to your API documentation changes.
7. Build the project by selecting the Build Project icon as seen in the screen capture above. If there are errors, you will see them in the build output.

### Troubleshooting Maven builds

If you get an error like this, it means that IntelliJ is unable to find your installation of Maven. 

![](images/maven-errors.png)

You will need to point IntelliJ to the Maven installation on your machine. You can update the path to your Maven
installation in your IntelliJ settings (**File** > **Settings**):

![](images/maven-errors-2.png)

## Updating the API documentation

This section will give you the information you need to update the API documentation in IntelliJ.

### Determining the Git/GitLab repository for an API

For all API documentation that's displayed/previewed in Confluence, you will need to determine the source repository in GitLab so that you can update the API documentation in IntelliJ.

**Steps:**

1. For a given wiki page in Confluence such as [https://wiki.dematic.com/display/IQH/ORD-EDGE+REST+API](https://wiki.dematic.com/display/IQH/ORD-EDGE+REST+API), display the attachments for the page (View attachments).

    ![](images/open-api-confluence.png)

    In the above screen capture, you can see the path to the Horizon repository for the generated API documentation. The displayed path indicates the location of the repository in GitLab.

2. Once you know the Git repository, you can work with that repository in IntelliJ and update the API documentation. Remember, you already cloned all the Horizon repositories in a previous section of this document. For more information, see [Clone all Horizon repositories](#clone-all-horizon-repositories).


### Searching the API documentation

IntelliJ Advanced Search and Notepad++ are useful tools to find API documentation content in the source code. You may want to add help content, update existing help content or make other edits. These tools make it easier to find what you need in a repository that may contain thousands of files. After updating the documentation, you can review your changes by running another Maven build and displaying the updated API documentation using an IntelliJ plugin. This will ensure that you have not introduced errors into the source code and the help content is satisfactory. 

#### Using Advanced Search in IntelliJ

You can use advanced search in IntelliJ to search for documentation in a single repository.

**Steps:**
    
1. Perform an advanced search like this in IntelliJ.  Select the root of the repository you are searching and select **CTRL-SHIFT-F** on your keyboard. This will display the search dialog in IntelliJ.  Here you can enter your search strings to find the API documentation you are looking for.
    
    This is the root of the repository:
    
    ![](images/root-repository.png)
    
    This is the Advanced Search window:
    
    ![](images/advanced-search-api.png)

    **Note**: It's also more efficient to use a file mask of _.java_ in your advanced search as it will narrow the search results to the files you need to update.
    
2. If you don't find the documentation you are looking for, you will need to search across all repositories; it's recommended to use Notepad++ due to CPU and RAM resource constraints. See [this topic](#using-notepad-to-search-across-all-horizon-projects) for more information.

#### Using Notepad++ to search across all Horizon projects

This method is more effective and less CPU and RAM intensive. If you prefer another text editor with
similar functions, please feel free to use it. 

Using [Notepad++](https://notepad-plus-plus.org/), select **Search** > **Find in 
Files** and then select the options in the screen capture below. Notepad++ will search across all your 
Horizon repositories. Once
you identify the repository to update, you should open the repository and edit the API documentation in IntelliJ.

**Note**: It may also make sense to filter your search results based on the _.java_ file extension. This will narrow your
search results and make the process more efficient.

Notepad++ search settings example:

![](images/notepadplusplus-find-in-files.png)

This is the same documentation in Swagger format:

![](images/swagger-pick-slot.png)


### Validating your API documentation updates

You can validate your API documentation updates with a Maven build and also with a Horizon checkstyle scan. You can then display the updated API documentation in IntelliJ with a plugin.

#### Running a build with Maven to validate your API documentation updates

You can validate your API documentation updates by running a build with Maven.

Steps: 

1. After making your changes in the API documentation:

    ![](images/api-update.png)

2. Ensure there are no errors in the file. Errors usually appear like this:

    ![](images/api-error-1.png)

    Another example of an error:

    ![](images/api-error-2.png)

3. After you've made your API documentation updates, you can run a build with Maven to ensure your changes don't result in errors. Run the `mvn clean install -DskipTests` command as outlined in this topic: [Run a build with Maven](#run-a-build-with-maven).  You should get a result like this:

    ![](images/maven-build-success.png)

    If you get errors like these during the build, you will need to fix the error and run the build again until you get a successful Maven build:

    ![](images/maven-error-1.png)

    Another example of an error:

    ![](images/maven-error-2.png)

4. If the build is successful, you can display the help using the **42Crunch** IntelliJ plugin. You can find the generated _.yaml_ file in the _target_ directory:

    ![](images/open-api-target-directory.png)

5. Click the Preview button to open the generated _.yaml_ file in your browser:

    ![](images/preview-api-1.png)

6. This is what the file will look like in your browser:

    ![](images/preview-api-2.png)


#### Scanning the code with Dematic checkstyle

You should also ensure once again that your changes agree with the Dematic checkstyle configuration.

**Steps:**

* Select a Java file that you updated and do a scan to test your updates against the Dematic coding standards. If your changes obey Horizon coding standards, you should get a message that there are no problems in the file.

   ![](images/horizon_checkstyle.png)


#### Display your updated API documentation in IntelliJ with a plugin

You can use a plugin to display the generated API documentation in IntelliJ.  This allows you to search the help file and test your content updates before committing your changes.

**Steps:**

1. Install this plugin by **42Crunch** that allows you to view and edit OpenAPI specification files:

    ![](images/42crunch.png)

2. You can find the latest OpenAPI specification file (_.yaml_) from your Maven build in the root of your _target_ directory:

    ![](images/open-api-target-directory.png)
   
3. To view the specification file (_.yaml_) in your browser, click the preview button:

    ![](images/plugin-preview.png)

4. The corresponding API documentation (in Swagger format and viewed with the plugin) will look like this:

   ![](images/api-swagger.png)

## Committing your API documentation changes

After completing your changes in the API documentation, you must follow the procedures in this section to commit
your files and merge your changes into the `main/master` branch of the project.

### Create a branch with your changes

You can create a branch in Git once you've finished making your changes to the API documentation or even before you start. 
It's your choice.

**Steps to create a branch in IntelliJ:**

1. Select **Git > New Branch**.
    
    ![](images/create-branch-1.png)
    
2. Name the branch (no spaces in the name) and check it out. As of writing, there are no standards for naming the branch. Check out the branch as part of creating it.
    
    ![](images/create-branch-2.png)
    
3. Commit your changes to the branch. Add a comment and select **Commit and Push**. This will push your changes to GitLab.
    
    ![](images/create-branch-2.5.png)
4. Click **Push** to send your changes to the remote repository in GitLab.

   ![](images/create-branch-3.png)
5. If you log in to GitLab, you will see a prompt for creating a Merge Request.

   ![](images/mr-1.png)

#### Determining the current branch

You can verify the branch you are currently working with by looking at these areas in the IDE:

![](images/create-branch-4.png)

You can also use the `git status` command in the terminal window:

![](images/git-status2.png) 

#### Switching between branches

At various times, you will need to switch between your personal branch and the `master/main` branch.

The IntelliJ help also has information on this procedure:

* [https://www.jetbrains.com/help/idea/manage-branches.html#switch-branches](https://www.jetbrains.com/help/idea/manage-branches.html#switch-branches)

**Steps:**

1. In the **Git** tab in IntelliJ, right-click the branch you want to switch to and select **Checkout**.

   ![](images/switch-branch-1.png)
2. The IntelliJ UI will now tell you that you are on that particular branch:

   ![](images/switch-branch-2.png)
3. You can also use `git status` command to see which branch you are currently working with.

   ![](images/git-status.png)

## Create a Merge Request (MR) in GitLab

[//]: # (TODO: This section will need to be updated once we are more sure on the process for making these updates.)


You'll need to create an MR in GitLab to have your changes merged into the `main/master` branch of the Git repository you are updating.

**Steps:**

1. After creating your branch and pushing your changes to GitLab, you will see this prompt in GitLab:

    ![](images/mr-1.png)
2. Click **Create merge request** and fill out this information with `Draft:` at the beginning of the title by selecting the **Mark as draft** check box. You may also want to assign a reviewer for the first few times you make changes. You can skip the reviewer on future updates if everyone becomes comfortable with the process.

    ![](images/merge-request-1.png)
    ![](images/merge-request-2.png)

3. Assign a reviewer if you aren't quite comfortable with the process. Depending on the level of comfort and your familiarity with the process, you may be able to skip this step in the future.
    
    ![](images/merge-request-3.png)

4. The reviewer will get an email courtesy of GitLab to review your MR. If everything is good,
   they will mark the merge request as **Ready** and merge your changes into the `main/master` branch.

**Note**: The process for updating API documentation is quite new at the moment and subject to change.

In the future, it may be possible to:

* Not have a reviewer or possibly just have another Technical Writer as a reviewer.

* Create the MR with `Ignore:` in the **Title** of the MR and mark the MR as **Ready**. This will tell Jenkins to merge this request automatically during the R&D build process.

## Determining if the build was successful in Jenkins

You can check if your MR resulted in a successful build in Jenkins. Here's an example. You can use similar paths for other SCMs: 

* [https://cje.dematic.com/teams-horizon/job/horizon/job/scm/job/ord/job/ord-public/](https://cje.dematic.com/teams-horizon/job/horizon/job/scm/job/ord/job/ord-public/)

If the Jenkins build is successful, you will see this:

![](images/jenkins-pipeline2.png)

# API standards at Dematic

**Note**: This section will require further updates as Technical Writers update and create standards for the API documentation.

You can use some methods mentioned here to apply Dematic standards to the API documentation:

* Go through the Terms list and ensure you are following standards: 
    * [https://kionnam.sharepoint.com/sites/scs_terms/Lists/glossary/AllItems.aspx](https://kionnam.sharepoint.com/sites/scs_terms/Lists/glossary/AllItems.aspx)

* Find errors in the `.yaml` file using IntelliJ:
    
    ![](images/error-IDE.png)
    
* Go through the functional product documentation in the Dematic wiki and apply the same standards to the API documentation. If you find terminology that's consistently misused, you can update each repository one at a time:  

* [https://wiki.dematic.com/display/IQH/Business+processes+-+Functional+product+documentation#](https://wiki.dematic.com/display/IQH/Business+processes+-+Functional+product+documentation#)

## API Style Guide at Dematic

**THIS SECTION IS UNDER CONSTRUCTION**

<!-- TODO:

* Can build this as we go.
* Can reference a wiki page as well.

 -->


## Learning more about API documentation

This is an excellent tutorial/course/workshop on REST APIs' by Tom Johnston that's highly recommended:

* [https://idratherbewriting.com/learnapidoc/](https://idratherbewriting.com/learnapidoc/)
* [A video version of the workshop above](https://idratherbewriting.com/blog/video-recordings-for-raleigh-api-documentation-workshop/)


## Linting the API documentation with Redocly CLI

**THIS SECTION IS UNDER CONSTRUCTION**

* Install Node.js for this.
* Install Redocly CLI, the free version.






